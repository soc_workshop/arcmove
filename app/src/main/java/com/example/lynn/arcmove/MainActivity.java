package com.example.lynn.arcmove;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private MyView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = new MyView(this);

        setContentView(new MyView(this));
    }

    public void onDestroy() {
        super.onDestroy();

        view.stop();
    }
}
