package com.example.lynn.arcmove;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * Created by lynn on 3/30/2017.
 */

public class MyView extends View implements Runnable {
    private int startAngle;
    private Paint paint;
    private Paint paint1;
    private boolean keepGoing;
    private Thread thread;

    public MyView(Context context) {
        super(context);

        Drawable drawable = getResources().getDrawable(R.drawable.background);

        setBackgroundDrawable(drawable);

        paint = new Paint();

        paint.setColor(0xFF0000CC);

        paint1 = new Paint();

        paint1.setColor(0xFFFFFFFF);

        keepGoing = true;

        thread = new Thread(this);

        thread.start();
    }

    public void stop() {
        keepGoing = false;
    }

    private void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    @Override
    public void run() {
        while (keepGoing) {
            this.post(new Runnable() {

                @Override
                public void run() {
                    invalidate();
                }
            });

            pause(0.1);

            startAngle += 10;
        }

    }

    public void onDraw(Canvas canvas) {
        canvas.drawCircle(500,500,250,paint);

        canvas.drawArc(new RectF(250,250,750,750),startAngle,20,true,paint1);


    }
}
